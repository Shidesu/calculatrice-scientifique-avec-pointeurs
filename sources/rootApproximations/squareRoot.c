//
// Created by Kao on 16/10/2018.
//

#include "headers/math.h"

double squareRoot(double a){
    double startingValue;
    double tempDouble = a;
    int counter = 0;
    double result = 0;

    while(tempDouble > 1){
        tempDouble /= 10.0;
        counter++;
    }

    if(a < 10){
        startingValue = 2 * (long)pow(10, counter);
    }
    else{
        startingValue = 6 * (long)pow(10, counter);
    }

    for (int i = 0; i < 100; i++){
        startingValue = 0.5 * (startingValue + a / startingValue);
    }

    result = startingValue;

    return result;
}