//
// Created by Kao on 17/10/2018.
//

//
// Created by Kao on 16/10/2018.
//

#include "headers/math.h"

double nRoot(double a, double n) {
    double startingValue;
    double tempDouble = a;
    int counter = 0;
    double result = 0;

    while (tempDouble > 1) {
        tempDouble /= 10.0;
        counter++;
    }

    if (a < 10) {
        startingValue = 2 * (long) pow(10, counter);
    } else {
        startingValue = 6 * (long) pow(10, counter);
    }

    for (int i = 1; i <= 100; i++) {
        startingValue = 1.0 / n * ((n - 1) * startingValue + a / pow(startingValue, n - 1));
    }

    result = startingValue;

    return result;
}