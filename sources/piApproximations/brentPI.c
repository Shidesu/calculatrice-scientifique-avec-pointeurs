//
// Created by Kao on 16/10/2018.
//

#include "headers/math.h"

double suiteA(double a, double b);

double suiteB(double a, double b);

double suiteT(double a, double a1, double t, double p);

double suiteP(double p);

double suiteBrent(double *a, double *b, double *t, double *p);

double brentPI(double n) {
    double a = 1;
    double b = 1 / squareRoot(2);

    double t = 1.0 / 4;
    double p = 1;
    double pi = 0;

    for (int i = 0; i < n; i++){
        pi = suiteBrent(&a, &b, &t, &p);
    }

    return pi;

}

double suiteBrent(double *a, double *b, double *t, double *p){
    double pi = 0;
    double aTemp = *a;

    *a = suiteA(*a, *b);
    *b = suiteB(aTemp, *b);
    *t = suiteT(aTemp, *a, *t, *p);
    *p = suiteP(*p);

    pi = pow((*a) + (*b), 2) / (4 * (*t));

    return pi;
}

double suiteA(double a, double b) {
    return (a + b) / 2;
}

double suiteB(double a, double b) {
    return squareRoot(a * b);
}

double suiteT(double a, double a1, double t, double p){
    return t - p * pow(a - a1, 2);
}

double suiteP(double p){
    return 2 * p;
}
