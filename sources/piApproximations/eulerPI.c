//
// Created by Kao on 16/10/2018.
//
#include "headers/math.h"
double eulerPI(double n) {
    double pi = 0;
    double numerateur = 0;
    double denominateur = 0;
    double temp = 0;

    for (int i = 1; i < n + 1; i++) {

        numerateur = 1;
        denominateur = (float) i * i;

        pi += numerateur / denominateur;
        temp = squareRoot(pi * 6.0);
    }

    return temp;

}