//
// Created by Kao on 16/10/2018.
//
#include "headers/simpleOperations.h"

double leibnizPI(double n) {
    double pi = 0;
    double numerateur = 0;
    double denominateur = 0;

    for (int i = 0; i < n; i++) {

        numerateur = pow(-1.0, i);
        denominateur = 2 * i + 1;

        pi += numerateur / denominateur;
    }

    return pi * 4;
}