//
// Created by Kao on 17/10/2018.
//

double factorielle(double number){
    unsigned long uNumber = number;
    double result = 0;

    if(uNumber == 0){
        result = 1;
    }
    else{
        result = (uNumber * factorielle(uNumber - 1));
    }
    return result;
}