//
// Created by Kao on 16/10/2018.
//

double pow(double a, double n){
    double result = 1;
    for(int i = 0; i < n; i++){
        result *= a;
    }
    return result;
}