//
// Created by Kao on 15/10/2018.
//

#include <headers/math.h>
#include "headers/utils.h"

void chosePIApprox(double (**chosenOperation)(double num1, double num2), Operations *operations);

bool choseOperation(double (**chosenOperation)(double num1, double num2), enum OperationsNames *operationName) {

    Operations operations;

    loadingOperations(&operations);

    while (*operationName == Unset) {

        printf("Veuillez-choisir l'op�ration � effectuer : \n");
        printf("0 : Quitter\n1 : Addition\n2 : Soustraction\n3 : Division\n4 : Multiplication\n5 : Approximation de PI\n6 : Racine carr�e\n7 : Racine n-i�me\n8 : Les puissances\n9 : Factorielle\n");

        scanf("%d", operationName);
        while ((getchar()) != '\n');//cleaning the input buffer

        if (*operationName > Unset || *operationName < Quitter)
            *operationName = Unset;

        switch (*operationName) {
            case Unset:
                printf("Op�ration non support�e !\n");
                break;
            case Quitter:
                return false;
                break; //Useless as we're using return, but keeping it for the syntax clarity.
            case Addition:
                *chosenOperation = operations.addition;
                break;
            case Soustraction:
                *chosenOperation = operations.soustraction;
                break;
            case Multiplication:
                *chosenOperation = operations.multiplication;
                break;
            case Division:
                *chosenOperation = operations.division;
                break;
            case ApproximationPI:
                chosePIApprox(chosenOperation, &operations);
                break;
            case RacineCarree:
                *chosenOperation = operations.racineCaree;
                break;
            case RacineNieme:
                *chosenOperation = operations.racineNieme;
                break;
            case Puissance:
                *chosenOperation = operations.puissance;
                break;
            case Factorielle:
                *chosenOperation = operations.factorielle;
                break;
            default:
                return false;
                break;


        }
    }

    return true;
}

void chosePIApprox(double (**chosenOperation)(double num1, double num2), Operations *operations) {
    int chosenPIApprox = 0;

    printf("Veuillez choisir la m�thode d'approximation de PI :\n1 : Leibniz\n2 : Euler\n3 : Brent-Salmin\n ");
    scanf("%d", &chosenPIApprox);

    switch (chosenPIApprox) {
        case 1:
            *chosenOperation = operations->leibnizPI;
            break;
        case 2:
            *chosenOperation = operations->eulerPI;
            break;
        case 3:
            *chosenOperation = operations->brentPI;
        default:
            return;
            break;
    }

}

void choseNumber(double *number1, double *number2, enum OperationsNames operationName) {
    printf("Entrez le premier nombre :\n");
    scanf("%lf", number1);
    while ((getchar()) != '\n');//cleaning the input buffer
    if (operationName == RacineCarree || operationName == ApproximationPI || operationName == Factorielle)
        return;
    printf("Le second : \n");
    scanf("%lf", number2);
    while ((getchar()) != '\n');//cleaning the input buffer
}


bool loadingOperations(Operations *operations) {

    operations->addition = addition;
    operations->soustraction = soustraction;
    operations->multiplication = multiplication;
    operations->division = division;
    operations->puissance = pow;
    operations->leibnizPI = leibnizPI;
    operations->eulerPI = eulerPI;
    operations->brentPI = brentPI;
    operations->racineCaree = squareRoot;
    operations->racineNieme = nRoot;
    operations->factorielle = factorielle;

}