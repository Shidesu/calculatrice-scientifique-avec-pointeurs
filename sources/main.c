#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "headers/utils.h"
#include "headers/math.h"

int main() {
    while (true) {
        double number1 = 0;
        double number2 = 0;
        double result = 0;
        enum OperationsNames operationName = Unset;

        double (*chosenOperation)(double num1, double num2) = NULL; //pointer to a function.

        if (!choseOperation(&chosenOperation, &operationName))
            return 0; //choseOperation is false so we're exiting the program.


        choseNumber(&number1, &number2, operationName); //passing vars by reference so we can change their value

        result = (*chosenOperation)(number1,
                                    number2); //calling the operation "returned" by choseOperation and storing the result.

        printf("Le r�sultat est: %lf\n", result); //displaying the result
    }
}