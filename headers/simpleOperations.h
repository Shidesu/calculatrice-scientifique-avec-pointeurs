//
// Created by Kao on 15/10/2018.
//

#ifndef CALCULATRICEPOINTEURSDEFONCTION_SIMPLEOPERATIONS_H
#define CALCULATRICEPOINTEURSDEFONCTION_SIMPLEOPERATIONS_H

double multiplication(double number1, double number2);

double addition(double number1, double number2);

double soustraction(double number1, double number2);

double division(double number1, double number2);

double pow(double a, double n);

double factorielle(double number);

#endif //CALCULATRICEPOINTEURSDEFONCTION_SIMPLEOPERATIONS_H
