//
// Created by Kao on 16/10/2018.
//

#ifndef CALCULATRICEPOINTEURSDEFONCTION_MATH_H
#define CALCULATRICEPOINTEURSDEFONCTION_MATH_H

#include "simpleOperations.h"

#include "piApproximations.h"

double squareRoot(double a);
double nRoot(double a, double n);

#endif //CALCULATRICEPOINTEURSDEFONCTION_MATH_H
