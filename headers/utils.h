//
// Created by Kao on 15/10/2018.
//

#ifndef CALCULATRICEPOINTEURSDEFONCTION_UTILS_H
#define CALCULATRICEPOINTEURSDEFONCTION_UTILS_H

#include <stdbool.h>
#include <stdio.h>


typedef struct Operations Operations;

struct Operations {
    double (*addition)(double num1, double num2);

    double (*soustraction)(double num1, double num2);

    double (*multiplication)(double num1, double num2);

    double (*division)(double num1, double num2);

    double (*racineCaree)(double num1, double num2);

    double (*racineNieme)(double a, double exposant);

    double (*puissance)(double a, double exposant);

    double (*leibnizPI)(double a, double useless);

    double (*eulerPI)(double a, double useless);

    double (*brentPI)(double a, double useless);

    double (*factorielle)(double a, double useless);

};

enum OperationsNames {
    Quitter,
    Addition,
    Soustraction,
    Multiplication,
    Division,
    ApproximationPI,
    RacineCarree,
    RacineNieme,
    Puissance,
    Factorielle,
    Unset,
};

bool choseOperation(double (**chosenOperation)(double num1, double num2), enum OperationsNames *operationsName);

void choseNumber(double *number1, double *number2, enum OperationsNames operationName);

bool loadingOperations(Operations *operations);


#endif //CALCULATRICEPOINTEURSDEFONCTION_UTILS_H
